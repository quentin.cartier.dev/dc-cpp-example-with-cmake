# README

This example is taken from the examples provided in kos sdk for sega dreamcast. (To be precise : /kos/examples/cpp/gltest)
I have only adapted it for use with cmake.

## What inside ?
- Defining a custom local toolkit from the toolchain file for dreamcast
- Sourcing the /opt/toolchains/dc/kos/environ.sh in the toolkit definition
- Add CMakeLists.txt to project
    - Build target
    - Build the romdisk
    - Create a cdi image

